package guru.xgm.somefire.dangerinspace;

import com.badlogic.gdx.utils.Array;

public class Location{

	private String name;
	public Array<Location> neighbours;
	public Array<NPC> npcs;
	private boolean isSpored;
	private boolean isInfected;
	private int oxygen;
	private int durability;
	
	public Location(String name){
		this.name = name;
		oxygen = 100;
		durability = 100;
		isSpored = false;
		isInfected = false;
		neighbours = new Array<Location>();
		npcs = new Array<NPC>();
	}

	public String getName(){
		return name;
	}

	public boolean isSpored(){
		return isSpored;
	}

	public void setSpored(boolean isSpored){
		this.isSpored = isSpored;
	}

	public boolean isInfected(){
		return isInfected;
	}

	public void setInfected(boolean isInfected){
		this.isInfected = isInfected;
	}

	public int getOxygen(){
		return oxygen;
	}

	public void setOxygen(int oxygen){
		this.oxygen = oxygen;
	}
	
	public void addOxygen(int value){
		this.oxygen += value;
		if (this.oxygen<0){
			this.oxygen = 0;
			
		}
	}

	public int getDurability(){
		return durability;
	}

	public void setDurability(int durability){
		this.durability = durability;
	}

	public void addDurability(int value){
		this.durability += value;
		if (this.durability<0) this.durability = 0;
	}
	
	public boolean hasCrew(){
		return npcs.size>0;
	}
	
	public boolean hasInfectedCrew(){
		for (NPC npc: npcs) if (npc.isInfected() && npc.isAlive()) return true;
		return false;
	}
	
	public boolean hasNotInfectedCrew(){
		for (NPC npc: npcs) if (!npc.isInfected() && npc.isAlive()) return true;
		return false;
	}
	
	public int whatCrew(){
		int i = 0, ni = 0;
		for (NPC npc: npcs){
			if (npc.isInfected() && npc.isAlive()) i++;
			if (!npc.isInfected() && npc.isAlive()) ni++;
		}
		if (i==0 && ni>1) return 2;
		if (i==0 && ni==1) return 1;
		if (i==1 && ni==0) return -1;
		if (i>1 && ni==0) return -2;
		if (i>0 && ni>0) return 3;
		return 0;
	}
}
