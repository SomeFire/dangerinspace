package guru.xgm.somefire.dangerinspace;

import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.ObjectSet;

public class NPC{

	private String name;
	private boolean isInfected;
	private Location location;
	private String job;
	private JobList jobList;
	private boolean isAlive;
	private boolean canMove;
	
	public NPC(String name){
		this.name = name;
		isInfected = false;
		isAlive = true;
		canMove = false;
	}

	public String getName(){
		return name;
	}

	public boolean isInfected(){
		return isInfected;
	}

	public void setInfected(boolean isInfected){
		this.isInfected = isInfected;
	}

	public Location getLocation(){
		return location;
	}

	public void setLocation(Location location){
		this.location = location;
	}

	public String getJob(){
		return job;
	}

	public void setJob(String job){
		this.job = job;
	}
	
	public void randomizeJob(){
		this.job = jobList.getRandomJob();
	}

	public JobList getJobList(){
		return jobList;
	}

	public void setJobList(JobList jobList){
		this.jobList = jobList;
	}

	public boolean isAlive(){
		return isAlive;
	}

	public void setAlive(Boolean isAlive){
		this.isAlive = isAlive;
	}
	
	public boolean canMove(){
		return canMove;
	}
	
	public void setMove(boolean b){
		canMove = b;
	}

	public void moveTo(Location destination){
		if (canMove){
			location.npcs.removeValue(this, false);
			destination.npcs.add(this);
			location = destination;
			canMove = false;
		}
	}
	
	public void moveToNextLocationInPath(){
		if (!location.getName().equals(job)){
			Location l = location, loc;
			ObjectMap<Location, Location> map = new ObjectMap<Location, Location>();//<next, prev>
			Array<Location> set = new Array<Location>();
			for (Location neightbour: l.neighbours){
				set.add(neightbour);
				map.put(neightbour, l);
			}
			// find destination loc
			int i = 0;
			while (i<set.size && !(l = set.get(i)).getName().equals(job)){
				for (Location neightbour: l.neighbours){
					if (!set.contains(neightbour, false)){
						set.add(neightbour);
						map.put(neightbour, l);
					}
				}
				i++;
			}
			// go back and find next loc
			while ((loc = map.get(l)) != location) {
				l = loc;
			}
			map.clear();
			set.clear();
			moveTo(l);
		}else{
			randomizeJob();
			canMove = false;
		}
	}
}
