package guru.xgm.somefire.dangerinspace;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import guru.xgm.somefire.questUI.AnswerButton;
import guru.xgm.somefire.questUI.AnswerButtonListener;
import guru.xgm.somefire.questUI.BGObjectMap;
import guru.xgm.somefire.questUI.BackButton;
import guru.xgm.somefire.questUI.Background;
import guru.xgm.somefire.questUI.GameTextObject;
import guru.xgm.somefire.questUI.NextButton;
import guru.xgm.somefire.questUI.QScreen;
import guru.xgm.somefire.questUI.TextWindow;
import guru.xgm.somefire.questUI.QUtils;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Pixmap.Blending;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Gdx2DPixmap;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.Button.ButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.CheckBox;
import com.badlogic.gdx.scenes.scene2d.ui.Dialog;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton.ImageButtonStyle;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.SelectBox;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Slider;
import com.badlogic.gdx.scenes.scene2d.ui.SplitPane;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.ui.TextField;
import com.badlogic.gdx.scenes.scene2d.ui.TextField.TextFieldListener;
import com.badlogic.gdx.scenes.scene2d.ui.VerticalGroup;
import com.badlogic.gdx.scenes.scene2d.ui.Window;
import com.badlogic.gdx.scenes.scene2d.utils.ChangeListener;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.badlogic.gdx.scenes.scene2d.utils.SpriteDrawable;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;
import com.badlogic.gdx.utils.Align;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.ObjectMap;
import com.badlogic.gdx.utils.viewport.ScreenViewport;

public class DangerInSpace extends ApplicationAdapter implements QScreen{
	SpriteBatch batch;
	BitmapFont font;
	Skin skin;
	Stage stage;
	TextWindow textWindow;
	NextButton nextButton;
	//BackButton backButton;
	TextButton
		menu,
		//showBG,
		help,
		//map,
		stats;
	float grw, grh;
	Table leftButtons, rightButtons;
	ScrollPane answersScrollPane;
	VerticalGroup answersGroup;
	Array<AnswerButton> answers;
	ObjectMap<Integer, GameTextObject> gtoMap;
	ObjectMap<String, TextureRegion> tr;
	Background bg;
	BGObjectMap<String, TextureRegion> backgrounds;
	GameTextObject gto;
	int
		reproduceTimer,
		reproduceTimeLeft,
		repairTimer;
	Stats gameStats;
	TextureAtlas ta;
	Array<Location> locations;
	Array<NPC> npcs;
	Random rand;

	private String
		patternVarsInText = "\\$\\w+",
		patternVariableText="\\{'(\\$\\w+)=(\\w+) (\\d+|\\w+)'\\?'(.*?)':'(.*?)'\\}",
		patternExpression = "(\\$\\w+)=(\\w+) (\\d+|\\w+)",
		patternNonEngineArgument = "'(.*?)'";
	
	boolean theEnd, lose;
	TextureRegionDrawable trd;
	Texture t;
	private Array<TextureRegion> a;
	Music music;
	
	@Override
	public void create () {
		rand = new Random();
		QUtils.gameScreen = this;
		QUtils.skin = skin = new Skin(Gdx.files.internal("skin/uiskin.json"));
		batch = new SpriteBatch();
		font = skin.getFont("default-font");
		stage = new Stage(new ScreenViewport());
		Gdx.input.setInputProcessor(stage);
		grw = Gdx.graphics.getWidth();
		grh = Gdx.graphics.getHeight();
		gameStats = new Stats();
		
		backgrounds = new BGObjectMap<String, TextureRegion>();
		//addFloor("alienplanet");
		//addFloor("alienplanet2");
		//addFloor("alienship");
		//addFloor("alienship2");
		//addFloor("engineering");
		//addFloor("hivecity-slum");
		//addFloor("hivecity-slum-door");
		addFloor("medbay");
		addFloor("medbay_blood");
		//addFloor("moonscape");
		//addFloor("ship");
		addFloor("ship_fleet");
		//addFloor("ship_planetdestruction");
		//addFloor("ship_warp");
		//addFloor("ship-junkyard");
		//addFloor("ship-wreck");
		//addFloor("space-elevator");
		//addFloor("tek-floor-blue");
		//addFloor("tek-floor-orange");
		addFloor("SciFi_VN_Background_Element_8");
		addFloor("redshiprbg");
		//addWall("alienship-wall1");
		//addWall("alienship-wall2");
		//addWall("engineering-conduit");
		//addWall("engineering-core");
		//addWall("moonscape-base");
		//addWall("moonscape-fleet");
		addWall("ship-future");
		//addWall("ship-gothic");
		addWall("spaceelevator-fleet");
		//addWall("spaceelevator-fleetandstation");
		//addWall("spaceelevator-station");
		//addWall("tekfloor-wall1");
		//addWall("tekfloor-wall2");
		
		tr = new ObjectMap<String, TextureRegion>();
		ta = new TextureAtlas("icons/Elements.atlas");
		tr.put("spores", ta.findRegion("spores"));
		tr.put("infestation", ta.findRegion("infestation"));
		tr.put("one_human", ta.findRegion("one_human"));
		tr.put("many_human", ta.findRegion("many_human"));
		tr.put("one_infected", ta.findRegion("one_infected"));
		tr.put("many_infected", ta.findRegion("many_infected"));
		tr.put("many_all", ta.findRegion("many_all"));
		for (Texture t: ta.getTextures()){
			t.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		}
		
		
		new GTOList(gtoMap = new ObjectMap<Integer, GameTextObject>());

		
		bg = new Background(stage, null, null);
		textWindow = new TextWindow(stage, skin, 0, grh-300, grw, 300);
		leftButtons = new Table(skin);
		//leftButtons.setBackground("default-rect");
		leftButtons.setBounds(0, 0, 50, 100);
		stage.addActor(leftButtons);
		//backButton = new BackButton("Back", skin);
		//showBG=new TextButton("BG", skin);
		help = new TextButton("Help", skin);
		help.addListener(new ClickListener(){
			@Override
			public void clicked (InputEvent event, float x, float y) {
				Dialog d = new Dialog("Help", skin);
				d.key(Keys.ESCAPE, false);
				d.row().fill().expandX();
				d.add("You are virus. You must infect or kill all crew on the ship.").colspan(2);
				d.row().left();
				Image i1 = new Image(tr.get("spores"));
				d.add(i1).size(50);
				d.add(new Label(" - Spores here. They can infect humans.", skin));
				d.row().left();
				Image i2 = new Image(tr.get("infestation"));
				d.add(i2).size(50);
				d.add(new Label(" - Infestation. Have better chances to infect humans.", skin));
				d.row().left();
				Image i3 = new Image(tr.get("one_human"));
				d.add(i3).size(50);
				d.add(new Label(" - Lonely human.", skin));
				d.row().left();
				Image i4 = new Image(tr.get("one_infected"));
				d.add(i4).size(50);
				d.add(new Label(" - Lonely infected human.", skin));
				d.show(stage);
			}
		});
		menu = new TextButton("Menu", skin);
		leftButtons.row().size(50, 50);
		//leftButtons.add(backButton);
		//leftButtons.row().size(50, 50);
		//leftButtons.add(showBG);
		leftButtons.add(help);
		leftButtons.row().size(50, 50);
		leftButtons.add(menu);
		
		rightButtons = new Table(skin);
		//rightButtons.setBackground("default-rect");
		rightButtons.setBounds(grw-50, 0, 50, 100);
		stage.addActor(rightButtons);
		nextButton = new NextButton("Next", skin);
		stats = new TextButton("Stats", skin);
		stats.addListener(new ClickListener(){
			@Override
			public void clicked (InputEvent event, float x, float y) {
				Dialog d = new Dialog("Virus upgrades", skin);
				d.key(Keys.ESCAPE, false);
				d.row().left();
				if ((Boolean)gameStats.map.get("mobile")) d.add(new Label("You can move spores around the ship.", skin));
				d.row().left();
				if ((Boolean)gameStats.map.get("infestation")) d.add(new Label("You can create infestation from spores in the room.", skin));
				d.row().left();
				if ((Boolean)gameStats.map.get("unoxy")) d.add(new Label("Virus can survive in vacuum.", skin));
				d.row().left();
				if ((Boolean)gameStats.map.get("mind_controll")) d.add(new Label("You can order infected crew to turn off some systems.", skin));
				d.row().left();
				if ((Boolean)gameStats.map.get("aggressive")) d.add(new Label("You can order infected crew to attack other crew or ship systems.", skin));
				d.show(stage);
			}
		});
		//map = new TextButton("Map", skin);
		nextButton.addListener(new ClickListener(){
			@Override
			public void clicked (InputEvent event, float x, float y) {
				//TODO clicked
				int id = (answersGroup.hasChildren())&&(QUtils.selectedAnswer!=null) ?
						QUtils.selectedAnswer.getNextSlideID() :
						gto.getNextId();
				if (id>0){
					for (NPC npc: npcs)
						npc.setMove(true);
					answerActions();
					nonEngineAnswerActions();
					if (id==3) aiActions();
					if (!theEnd) changeTextObject(id);
					else{
						GameTextObject gto = new GameTextObject();
						gto.setId(4);
						int counter = 0;
						for (NPC npc: npcs) if (npc.isAlive()) counter++;
						if (lose){
							gto.setBgFloor("medbay");
							if (counter>0)
								gto.setText("The virus is died before it could infect all members of the ship."
									+ (counter!=4?"\nBut part of the crew died, and humanity is now ready to fight the virus.":""));
							else
								gto.setText("The virus killed all crew, but couldn't survive without them and died too.\n"
										+ "A new ghost-ship appears in the universe.");
						}else{
							gto.setBgFloor("medbay_blood");
							if (counter==4)
								gto.setText("The virus infected all crew of the scout ship.\n"
									+ "One more ship will bring death to humanity.");
							else
								gto.setText("The virus infected or killed crew of the scout ship.\n"
										+ "But part of the crew died, and remaining crew were sent to quarantine.");
						}
						gto.setBgWall(null);
						AnswerButton ab = new AnswerButton(0);
						ab.setText("Restart");
						gto.addAnswer(ab);
						ab = new AnswerButton(0);
						ab.setText("Exit game");
						ab.setAction("Exit game");
						gto.addAnswer(ab);
						gtoMap.put(4, gto);
						changeTextObject(4);
					}
				}
			}
		});
		rightButtons.row().size(50, 50);
		rightButtons.add(nextButton);
		rightButtons.row().size(50, 50);
		rightButtons.add(stats);
		//rightButtons.row().size(50, 50);
		//rightButtons.add(map);
		
		textWindow.setY(leftButtons.getTop());
		
		answersGroup = new VerticalGroup();
		answersGroup.fill();
		answersScrollPane = new ScrollPane(answersGroup, skin);
		answersScrollPane.setBounds(leftButtons.getRight(), 0, grw-leftButtons.getWidth()-rightButtons.getWidth(), leftButtons.getHeight());
		stage.addActor(answersScrollPane);

		answers = new Array<AnswerButton>();
		
		theEnd = false;
		lose = false;
		reproduceTimer = 5;
		reproduceTimeLeft = 5;
		repairTimer = 0;

		t = new Texture(960, 738, Pixmap.Format.RGBA8888);
		t.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		trd = new TextureRegionDrawable(new TextureRegion(t));
		
		a = new Array<TextureRegion>();
		
		music = Gdx.audio.newMusic(Gdx.files.internal("music/Exploring Planets.mp3"));
		music.setLooping(true);
		music.play();
		
		changeTextObject(0);
	}

	private void restart(){
		locations = new Array<Location>();
		locations.add(new Location("Engine Room"));
		locations.add(new Location("Cargo"));
		locations.add(new Location("Generator Room"));
		locations.add(new Location("Life Support Room"));
		locations.add(new Location("Living Room"));
		locations.add(new Location("Medbay"));
		locations.add(new Location("Pilot Room"));
		locations.add(new Location("Left Front Corridor"));
		locations.add(new Location("Left Rear Corridor"));
		locations.add(new Location("Right Front Corridor"));
		locations.add(new Location("Right Rear Corridor"));
		Location l;
		l = getLocation("Cargo");
		l.setSpored(true);
		l.neighbours.add(getLocation("Engine Room"));
		l.neighbours.add(getLocation("Generator Room"));
		l.neighbours.add(getLocation("Life Support Room"));
		l = getLocation("Engine Room");
		l.neighbours.add(getLocation("Cargo"));
		l.neighbours.add(getLocation("Left Rear Corridor"));
		l.neighbours.add(getLocation("Right Rear Corridor"));
		l = getLocation("Left Rear Corridor");
		l.neighbours.add(getLocation("Engine Room"));
		l.neighbours.add(getLocation("Left Front Corridor"));
		l = getLocation("Right Rear Corridor");
		l.neighbours.add(getLocation("Engine Room"));
		l.neighbours.add(getLocation("Right Front Corridor"));
		l = getLocation("Generator Room");
		l.neighbours.add(getLocation("Cargo"));
		l.neighbours.add(getLocation("Left Front Corridor"));
		l.neighbours.add(getLocation("Medbay"));
		l.neighbours.add(getLocation("Life Support Room"));
		l = getLocation("Life Support Room");
		l.neighbours.add(getLocation("Cargo"));
		l.neighbours.add(getLocation("Right Front Corridor"));
		l.neighbours.add(getLocation("Generator Room"));
		l.neighbours.add(getLocation("Living Room"));
		l = getLocation("Living Room");
		l.neighbours.add(getLocation("Life Support Room"));
		l.neighbours.add(getLocation("Medbay"));
		l.neighbours.add(getLocation("Right Front Corridor"));
		l.neighbours.add(getLocation("Pilot Room"));
		l = getLocation("Medbay");
		l.neighbours.add(getLocation("Left Front Corridor"));
		l.neighbours.add(getLocation("Generator Room"));
		l.neighbours.add(getLocation("Living Room"));
		l = getLocation("Pilot Room");
		l.neighbours.add(getLocation("Living Room"));
		l = getLocation("Left Front Corridor");
		l.neighbours.add(getLocation("Medbay"));
		l.neighbours.add(getLocation("Generator Room"));
		l.neighbours.add(getLocation("Left Rear Corridor"));
		l.neighbours.add(getLocation("Right Front Corridor"));
		l = getLocation("Right Front Corridor");
		l.neighbours.add(getLocation("Life Support Room"));
		l.neighbours.add(getLocation("Living Room"));
		l.neighbours.add(getLocation("Right Rear Corridor"));
		l.neighbours.add(getLocation("Left Front Corridor"));
		
		npcs = new Array<NPC>();
		NPC npc;
		JobList jl;
		npc = new NPC("Engineer");
		npc.setLocation(getRandomLocation());
		jl = new JobList();
		jl.add("Engine Room", 25);
		jl.add("Cargo", 5);
		jl.add("Generator Room", 5);
		jl.add("Life Support Room", 5);
		jl.add("Living Room", 1);
		//jl.add("Medbay", 0);
		//jl.add("Pilot Room", 0);
		jl.add("Left Front Corridor", 1);
		jl.add("Left Rear Corridor", 1);
		jl.add("Right Front Corridor", 1);
		jl.add("Right Rear Corridor", 1);
		npc.setJobList(jl);
		npc.randomizeJob();
		npcs.add(npc);
		npc = new NPC("Pilot");
		npc.setLocation(getRandomLocation());
		jl = new JobList();
		jl.add("Living Room", 5);
		jl.add("Medbay", 5);
		jl.add("Pilot Room", 25);
		npc.setJobList(jl);
		npc.randomizeJob();
		npcs.add(npc);
		npc = new NPC("Medic");
		npc.setLocation(getRandomLocation());
		jl = new JobList();
		jl.add("Living Room", 3);
		jl.add("Medbay", 15);
		jl.add("Pilot Room", 5);
		jl.add("Left Front Corridor", 3);
		jl.add("Left Rear Corridor", 3);
		jl.add("Right Front Corridor", 3);
		jl.add("Right Rear Corridor", 3);
		npc.setJobList(jl);
		npc.randomizeJob();
		npcs.add(npc);
		npc = new NPC("Captain");
		npc.setLocation(getRandomLocation());
		jl = new JobList();
		jl.add("Cargo", 5);
		jl.add("Living Room", 5);
		jl.add("Medbay", 5);
		jl.add("Pilot Room", 5);
		jl.add("Left Front Corridor", 3);
		jl.add("Left Rear Corridor", 3);
		jl.add("Right Front Corridor", 3);
		jl.add("Right Rear Corridor", 3);
		npc.setJobList(jl);
		npc.randomizeJob();
		npcs.add(npc);
	}

	//TODO ai
	private void aiActions(){
		decreasTime();
		// oxygen
		if (!(Boolean)gameStats.map.get("oxygenIsActive")) 
			for (Location l: locations){
				l.addOxygen(-5);
			}
		for (Location l: locations){
			if (l.getDurability() <= 0) l.addOxygen(-20);
			if (l.getOxygen() <= 0){
				if (!(Boolean)gameStats.map.get("unoxy")){
					l.setSpored(false);
					l.setInfected(false);
				}
				for (NPC npc: l.npcs){
					if (npc.isInfected() && (Boolean)gameStats.map.get("unoxy")){
					}else{
						npc.setAlive(false);
					}
				}
			}
		}
		// win/lose conditions
		int counter = 0;
		//all infected/dead
		for (NPC npc: npcs){
			if (npc.isInfected() || !npc.isAlive()) counter++;
		}
		if (counter==npcs.size){
			System.out.println("all dead/infected");
			theEnd = true;
			lose = false;
		}
		//infection alive
		boolean b = false;
		for (Location l: locations){
			if (l.isInfected()||l.isSpored()||l.hasInfectedCrew()) b = true;
		}
		if (!b){
			System.out.println("virus is dead");
			theEnd= true;
			lose = true;
		}

		if (!theEnd){
			// jobs
			setCurrentRepairer();
			for (NPC npc: npcs){
				if (npc.isAlive() && npc.canMove()){
					turnOnSmthng(npc);
				}
				if (npc.isAlive() && npc.canMove()){
					npc.moveToNextLocationInPath();
				}
			}
		}
	}

	private void setCurrentRepairer(){
		if (!(Boolean)gameStats.map.get("generatorIsActive") || !(Boolean)gameStats.map.get("oxygenIsActive") || !(Boolean)gameStats.map.get("engineIsActive")){
			NPC repairer = null, npc;
			if ((npc = getNPC("Engineer")).isAlive()) repairer = npc;
			if ((npc = getNPC("Captain")).isAlive() && repairTimer>5) repairer = npc;
			if ((npc = getNPC("Medic")).isAlive() && repairTimer>8) repairer = npc;
			if ((npc = getNPC("Pilot")).isAlive() && repairTimer>10) repairer = npc;
			if (repairer!=null){
				for (NPC n: npcs){
					if (n.getLocation().getName().equals("Generator Room") && !(Boolean)gameStats.map.get("generatorIsActive") && (Integer)gameStats.map.get("generatorHP")>0){
						n.setJob("Generator Room");
					}
					if (n.getLocation().getName().equals("Life Support Room") && (Boolean)gameStats.map.get("generatorIsActive") && !(Boolean)gameStats.map.get("oxygenIsActive") && (Integer)gameStats.map.get("oxygenHP")>0){
						n.setJob("Life Support Room");
					}
					if (n.getLocation().getName().equals("Pilot Room") && (Boolean)gameStats.map.get("generatorIsActive") && !(Boolean)gameStats.map.get("engineIsActive") && (Integer)gameStats.map.get("engineHP")>0){
						n.setJob("Engine Room");
					}
				}
			}
		}
	}

	private void turnOnSmthng(NPC npc){
		if (npc.getLocation().getName().equals("Generator Room") && !(Boolean)gameStats.map.get("generatorIsActive") && (Integer)gameStats.map.get("generatorHP")>0){
			gameStats.map.put("generatorIsActive", true);
			npc.setMove(false);
		}
		if (npc.getLocation().getName().equals("Life Support Room") && (Boolean)gameStats.map.get("generatorIsActive") && !(Boolean)gameStats.map.get("oxygenIsActive") && (Integer)gameStats.map.get("oxygenHP")>0){
			gameStats.map.put("oxygenIsActive", true);
			npc.setMove(false);
		}
		if (npc.getLocation().getName().equals("Pilot Room") && (Boolean)gameStats.map.get("generatorIsActive") && !(Boolean)gameStats.map.get("engineIsActive") && (Integer)gameStats.map.get("engineHP")>0){
			gameStats.map.put("engineIsActive", true);
			npc.setMove(false);
		}
	}

	private Location getRandomLocation(){
		return locations.get(rand.nextInt(locations.size));
	}
	
	@Override
	public void render () {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

		stage.act(Math.min(Gdx.graphics.getDeltaTime(), 1 / 30f));
		stage.draw();
		if (gto.getId()==3) renderBG();
	}

	@Override
	public void resize (int width, int height) {
		stage.getViewport().update(width, height, true);
	}

	@Override
	public void dispose () {
		stage.dispose();
		skin.dispose();
	}
	
	private void addFloor(String name){
		Texture t = new Texture(Gdx.files.internal("floors-highres/"+name+".png"));
		t.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		backgrounds.put(name, new TextureRegion(t));
	}
	
	private void addWall(String name){
		Texture t = new Texture(Gdx.files.internal("walls-highres/"+name+".png"));
		t.setFilter(TextureFilter.Linear, TextureFilter.Linear);
		backgrounds.put(name, new TextureRegion(t));
	}
	
	private void decreasTime(){
		reproduceTimeLeft--;
		if (reproduceTimeLeft<0) reproduceTimeLeft = 0;
	}
	
	private void renderRoom(String name, int x, int y){
		a.clear();
		if (getLocation(name).isSpored()) a.add(tr.get("spores"));
		if (getLocation(name).isInfected()) a.add(tr.get("infestation"));
		int i = getLocation(name).whatCrew();
		switch (i){
		case 2:
			a.add(tr.get("many_human"));
			break;
		case 1:
			a.add(tr.get("one_human"));
			break;
		case -1:
			a.add(tr.get("one_infected"));
			break;
		case -2:
			a.add(tr.get("many_infected"));
			break;
		case 3:
			a.add(tr.get("many_all"));
			break;
		}
		i = 0;
		for (TextureRegion tr: a){
			batch.draw(tr, x+i*30, y, 26, 26);
			i++;
		}
	}
	
	private void renderRoomName(String name, int x, int y){
		font.draw(batch, name, x, y);
	}
	
	private void renderBG(){
		batch.begin();
		
		renderRoomName("Engine Room", 84, (int)grh-125);
		renderRoomName("Cargo", 230, (int)grh-125);
		renderRoomName("Generator", 365, (int)grh-95);
		renderRoomName("Life Support", 357, (int)grh-155);
		renderRoomName("Medbay", 485, (int)grh-95);
		renderRoomName("Living Room", 480, (int)grh-155);
		renderRoomName("Pilot Room", 595, (int)grh-125);
		renderRoomName("Left Rear Corridor", 100, (int)grh-60);
		renderRoomName("Left Front Corridor", 400, (int)grh-60);
		renderRoomName("Right Rear Corridor", 100, (int)grh-215);
		renderRoomName("Right Front Corridor", 400, (int)grh-215);
		
		renderRoom("Engine Room", 90, (int)grh-170);
		renderRoom("Cargo", 220, (int)grh-170);
		renderRoom("Generator Room", 360, (int)grh-135);
		renderRoom("Life Support Room", 360, (int)grh-200);
		renderRoom("Medbay", 480, (int)grh-135);
		renderRoom("Living Room", 480, (int)grh-200);
		renderRoom("Pilot Room", 599, (int)grh-170);
		renderRoom("Left Rear Corridor", 250, (int)grh-80);
		renderRoom("Left Front Corridor", 560, (int)grh-80);
		renderRoom("Right Rear Corridor", 250, (int)grh-235);
		renderRoom("Right Front Corridor", 560, (int)grh-235);
		
		batch.end();
	}
	
	private Pixmap p;
	
	private void updateBG(){
		//TODO room names
		p = new Pixmap(960, 738, Pixmap.Format.RGBA8888);
		Blending b = Pixmap.getBlending();
		Pixmap.setBlending(Blending.None);
		fillOxygen("Engine Room", 100, 180, 120, 100);
		fillOxygen("Cargo", 230, 180, 170, 100);
		fillOxygen("Generator Room", 420, 135, 130, 80);
		fillOxygen("Life Support Room", 420, 230, 130, 80);
		fillOxygen("Medbay", 570, 135, 100, 80);
		fillOxygen("Living Room", 570, 230, 130, 80);
		fillOxygen("Pilot Room", 710, 180, 100, 100);
		fillOxygen("Left Rear Corridor", 100, 80, 300, 50);
		fillOxygen("Left Front Corridor", 410, 80, 450, 50);
		fillOxygen("Left Front Corridor", 830, 80, 30, 140);
		fillOxygen("Right Rear Corridor", 100, 320, 300, 50);
		fillOxygen("Right Front Corridor", 410, 320, 450, 50);
		fillOxygen("Right Front Corridor", 830, 230, 30, 140);
		t.draw(p, 0, 0);
		tr.get("spores").getTexture();
		bg.getWall().setDrawable(trd);
		p.dispose();
		Pixmap.setBlending(b);
	}
	
	private void fillOxygen(String roomName, int x, int y, int w, int h){
		float oxygen = ((float)getLocation(roomName).getOxygen())/100;
		p.setColor(Color.rgba8888(1f, oxygen, oxygen, 1f));
		p.fillRectangle(x, y, w, h);
	}

	//TODO changeTextObject
	@Override
	public void changeTextObject(int id){
		if (id == 0) restart();
		gto = gtoMap.get(id);
		bg.setBackground(backgrounds.get(gto.getBgFloor()), backgrounds.get(gto.getBgWall()));
		
		if (id == 3) updateBG();
		
		boolean answerAvailable = true;
		String line = gto.getText();
		String newLine = line;
		String pattern = patternVariableText;
		String s;
		Pattern r = Pattern.compile(pattern);
		Matcher m = r.matcher(line);
		while (m.find()){
			answerAvailable = checkCondition(m.group(1), m.group(2), m.group(3));
			newLine = newLine.replace(m.group(), answerAvailable ? m.group(4) : m.group(5));
		}
		
		
		pattern = patternVarsInText;
		r = Pattern.compile(pattern);
		m = r.matcher(line);
		while (m.find()){
			s = m.group();
			newLine = newLine.replace(s, gameStats.map.get(s.substring(1)).toString());
		}
		textWindow.setText(newLine);
		
		answersGroup.clear();
		QUtils.selectedAnswer = null;
		for (AnswerButton answer:gto.getAnswers()){
			line = answer.getCondition();
			answerAvailable = true;
			if (line!=null && line!=""){
				newLine = line;
				pattern = patternExpression;
				r = Pattern.compile(pattern);
				m = r.matcher(line);
				while (m.find()&&answerAvailable){
					answerAvailable = checkCondition(m.group(1), m.group(2), m.group(3));
				}
			}
			answer.setChecked(false);
			if (answerAvailable) answersGroup.addActor(answer);
		}
		if (id==3) nonEngineAnswers();
	}
	
	private void answerActions(){
		if (	QUtils.selectedAnswer!=null &&
				QUtils.selectedAnswer.getAction()!=null &&
				QUtils.selectedAnswer.getAction()!="" &&
				QUtils.selectedAnswer.getAction().startsWith("$")){
			String //s,
				s1,s2,s3;
			String line = QUtils.selectedAnswer.getAction();
			String pattern = patternExpression;
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(line);
			while (m.find()){
				//s = m.group(0);
				s1 = m.group(1);
				s2 = m.group(2);
				s3 = m.group(3);
				Object variable = gameStats.map.get(s1.substring(1));
				String expression = s3;
				if (variable.getClass() == Integer.class){
					Integer a = (Integer)variable,
						b = Integer.valueOf(expression);
					if (s2.equals("inc")){
						a+=b;
					}else
					if (s2.equals("dec")){
						a-=b;
					}else
					if (s2.equals("set")){
						a=b;
					}
					gameStats.map.put(s1.substring(1), a);
				}else
				if (variable.getClass() == Boolean.class){
					if (s2.equals("set")){
						gameStats.map.put(s1.substring(1), Boolean.valueOf(expression));
					}
				}
			}
		}
	}

	private void nonEngineAnswerActions(){
		
		if (	QUtils.selectedAnswer!=null &&
				QUtils.selectedAnswer.getAction()!=null &&
				QUtils.selectedAnswer.getAction()!="" &&
				!QUtils.selectedAnswer.getAction().startsWith("$")){
			//System.out.println("inside nonEngineAnswerActions()");
			String //s,
				s1,s2,s3;
			String line = QUtils.selectedAnswer.getAction();
			String pattern = patternNonEngineArgument;
			Pattern r = Pattern.compile(pattern);
			Matcher m = r.matcher(line);
			Location l;
			NPC npc;
			if (line.equals("Exit game")){
				Gdx.app.exit();
			}else
			if (line.startsWith("infect ")){
				m.find();
				npc = getNPC(m.group(1));
				l = npc.getLocation();
				int chance = l.isInfected() ? 75 : 50;
				if (chance > rand.nextInt(100)){
					System.out.println(l.getName()+", "+l.isSpored());
					npc.setInfected(true);
					l.setSpored(false);
					System.out.println(l.getName()+", "+l.isSpored());
				}
			}else
			if (line.startsWith("move spores ")){
				m.find();
				l = getLocation(m.group(1));
				l.setSpored(false);
				m.find();
				l = getLocation(m.group(1));
				l.setSpored(true);
			}else
			if (line.startsWith("reproduce spores ")){
				m.find();
				l = getLocation(m.group(1));
				l.setSpored(true);
				reproduceTimeLeft = reproduceTimer;
			}else
			if (line.startsWith("infestation ")){
				m.find();
				l = getLocation(m.group(1));
				l.setSpored(false);
				l.setInfected(true);
			}else
			if (line.startsWith("attack crew in ")){
				m.find();
				l = getLocation(m.group(1));
				// count battlers
				int allies=0, enemies=0;
				for (NPC battler: l.npcs){
					battler.setMove(false);
					if (battler.isAlive()){
						if (battler.isInfected()) allies++;
						else enemies++;
					}
				}
				//TODO battle
				boolean battleResult = allies>=enemies;
				if (battleResult){
					// infected win
					for (NPC battler: l.npcs){
						if (!battler.isInfected()){
							battler.setAlive(false);
						}
					}
					if ((Boolean)gameStats.map.get("mobile")){
						l.setInfected(true);
					}
				}else{
					// infected lose
					for (NPC battler: l.npcs){
						if (battler.isInfected()){
							battler.setAlive(false);
						}
					}
					if ((Boolean)gameStats.map.get("mobile") || (Boolean)gameStats.map.get("strong")){
						l.setInfected(true);
					}
				}
			}else
			if (line.startsWith("move crew ")){
				m.find();
				npc = getNPC(m.group(1));
				m.find();
				l = getLocation(m.group(1));
				npc.getLocation().npcs.removeValue(npc, false);
				npc.setLocation(l);
				l.npcs.add(npc);
				npc.setMove(false);
			}else
			if (line.equals("destroy generator")){
				l = getLocation("Generator Room");
				int hp = (Integer)gameStats.map.get("generatorHP");
				for (NPC battler: l.npcs){
					if (battler.isAlive() && battler.isInfected()){
						hp-=15;
						battler.setMove(false);
					}
				}
				if (hp<=0){
					hp = 0;
					gameStats.map.put("generatorIsActive", false);
					gameStats.map.put("oxygenIsActive", false);
					gameStats.map.put("engineIsActive", false);
				}
				gameStats.map.put("generatorHP", hp);
			}else
			if (line.equals("destroy oxygen")){
				l = getLocation("Life Support Room");
				int hp = (Integer)gameStats.map.get("oxygenHP");
				System.out.println(hp);
				for (NPC battler: l.npcs){
					if (battler.isAlive() && battler.isInfected()){
						hp-=15;
						battler.setMove(false);
					}
				}
				if (hp<=0){
					System.out.println("hp==0");
					hp = 0;
					gameStats.map.put("oxygenIsActive", false);
				}
				gameStats.map.put("oxygenHP", hp);
			}else
			if (line.equals("destroy engine")){
				l = getLocation("Engine Room");
				int hp = (Integer)gameStats.map.get("engineHP");
				for (NPC battler: l.npcs){
					if (battler.isAlive() && battler.isInfected()){
						hp-=15;
						battler.setMove(false);
					}
				}
				if (hp<=0){
					hp = 0;
					gameStats.map.put("engineIsActive", false);
				}
				gameStats.map.put("engineHP", hp);
			}else
			if (line.equals("off generator")){
				l = getLocation("Generator Room");
				gameStats.map.put("generatorIsActive", false);
				gameStats.map.put("oxygenIsActive", false);
				gameStats.map.put("engineIsActive", false);
				for (NPC n: l.npcs)
					if (n.isInfected()) n.setMove(false);
			}else
			if (line.equals("off oxygen")){
				l = getLocation("Life Support Room");
				gameStats.map.put("oxygenIsActive", false);
				for (NPC n: l.npcs)
					if (n.isInfected()) n.setMove(false);
			}else
			if (line.equals("off engine")){
				l = getLocation("Pilot Room");
				gameStats.map.put("engineIsActive", false);
				for (NPC n: l.npcs)
					if (n.isInfected()) n.setMove(false);
			}
		}
	}
	
	//TODO create nonEnginAnswers
	private void nonEngineAnswers(){
		//System.out.println("start nonEngineAnswers()");
		// add NONENGINE answers
		for (Location l: locations){
			//+try to infect crew
			System.err.println(l.getName()+" "+l.npcs.size);
			if ((l.isInfected()||l.isSpored()) && l.hasCrew()){
				for (NPC npc: l.npcs){
					if (!npc.isInfected() && npc.isAlive()){
						System.err.println(l.getName()+", "+npc.getName());
						AnswerButton answer = new AnswerButton(3);
						answer.setText("Try to infect "+npc.getName()+" in "+l.getName()+".");
						answer.setAction("infect '"+npc.getName()+"'");
						answersGroup.addActor(answer);
					}
				}
			}
			//reproduce spores
			if (reproduceTimeLeft==0 && !l.isSpored() && (l.isInfected() || l.hasInfectedCrew())){
				AnswerButton answer = new AnswerButton(3);
				answer.setText("Reproduce spores in "+l.getName()+".");
				answer.setAction("reproduce spores '"+l.getName()+"'");
				answersGroup.addActor(answer);
			}
			//+move spores
			if (l.isSpored()){
				for (Location neighbour: l.neighbours){
					if (!neighbour.isSpored() && (Boolean)gameStats.map.get("mobile")){
						AnswerButton answer = new AnswerButton(3);
						answer.setText("Move spores from "+l.getName()+" to "+neighbour.getName()+".");
						answer.setAction("move spores '"+l.getName()+"''"+neighbour.getName()+"'");
						answersGroup.addActor(answer);
					}
				}
				
				//create infestation in the room
				if (!l.isInfected() && (Boolean)gameStats.map.get("infestation")){
					AnswerButton answer = new AnswerButton(3);
					answer.setText("Create infestation in "+l.getName()+".");
					answer.setAction("infestation '"+l.getName()+"'");
					answersGroup.addActor(answer);
				}
			}
			//+give order to attack
			if ((Boolean)gameStats.map.get("aggressive") && l.hasInfectedCrew() && l.hasNotInfectedCrew()){
				AnswerButton answer = new AnswerButton(3);
				answer.setText("Attack crew in "+l.getName()+".");
				answer.setAction("attack crew in '"+l.getName()+"'");
				answersGroup.addActor(answer);
			}
			
		}
		//+order to break something
		Location l;
		l = getLocation("Generator Room");
		if ((Boolean)gameStats.map.get("aggressive") && l.hasInfectedCrew()){
			AnswerButton answer = new AnswerButton(3);
			answer.setText("Attack generator.");
			answer.setAction("destroy generator");
			answersGroup.addActor(answer);
		}
		if ((Boolean)gameStats.map.get("mind_controll") && l.hasInfectedCrew() && (Boolean)gameStats.map.get("generatorIsActive")){
			AnswerButton answer = new AnswerButton(3);
			answer.setText("Turn off generator.");
			answer.setAction("off generator");
			answersGroup.addActor(answer);
		}
		l = getLocation("Life Support Room");
		if ((Boolean)gameStats.map.get("aggressive") && l.hasInfectedCrew()){
			AnswerButton answer = new AnswerButton(3);
			answer.setText("Attack air treatment system.");
			answer.setAction("destroy oxygen");
			answersGroup.addActor(answer);
		}
		if ((Boolean)gameStats.map.get("mind_controll") && l.hasInfectedCrew() && (Boolean)gameStats.map.get("oxygenIsActive")){
			AnswerButton answer = new AnswerButton(3);
			answer.setText("Turn off air treatment system.");
			answer.setAction("off oxygen");
			answersGroup.addActor(answer);
		}
		l = getLocation("Engine Room");
		if ((Boolean)gameStats.map.get("aggressive") && l.hasInfectedCrew()){
			AnswerButton answer = new AnswerButton(3);
			answer.setText("Attack engine.");
			answer.setAction("destroy engine");
			answersGroup.addActor(answer);
		}
		l = getLocation("Pilot Room");
		if ((Boolean)gameStats.map.get("mind_controll") && l.hasInfectedCrew() && (Boolean)gameStats.map.get("engineIsActive")){
			AnswerButton answer = new AnswerButton(3);
			answer.setText("Turn off engine.");
			answer.setAction("off engine");
			answersGroup.addActor(answer);
		}
		//+order to move npc
		for (NPC npc: npcs){
			if (npc.isInfected() && npc.isAlive()){
				for (Location loc: npc.getLocation().neighbours){
					AnswerButton answer = new AnswerButton(3);
					answer.setText("Order "+npc.getName()+" to move to "+loc.getName());
					answer.setAction("move crew '"+npc.getName()+"''"+loc.getName()+"'");
					answersGroup.addActor(answer);
				}
			}
		}
		AnswerButton answer = new AnswerButton(3);
		answer.setText("Wait.");
		answersGroup.addActor(answer);
		//System.out.println("end nonEngineAnswers()");
	}

	private boolean checkCondition(String variable, String expression, String argument){
		Object variableObject = gameStats.map.get(variable.substring(1));
		if (variableObject.getClass() == Integer.class){
			if (expression.equals("above")){
				if ((Integer)variableObject>Integer.valueOf(argument)) return true;
			}else
			if (expression.equals("below")){
				if ((Integer)variableObject<Integer.valueOf(argument)) return true;
			}else
			if (expression.equals("equal")){
				if ((Integer)variableObject==Integer.valueOf(argument)) return true;
			}
		}else
		if (variableObject.getClass() == Boolean.class){
			if (expression.equals("equal")){
				if ((Boolean)variableObject==Boolean.valueOf(argument)) return true;
			}
		}
		return false;
	}

	private Location getLocation(String name){
		for (Location l: locations) if (l.getName().equals(name)) return l;
		return null;
	}
	
	private NPC getNPC(String name){
		for (NPC npc: npcs) if (npc.getName().equals(name)) return npc;
		return null;
	}
}
