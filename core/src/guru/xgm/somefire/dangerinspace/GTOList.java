package guru.xgm.somefire.dangerinspace;

import guru.xgm.somefire.questUI.AnswerButton;
import guru.xgm.somefire.questUI.AnswerButtonListener;
import guru.xgm.somefire.questUI.GameTextObject;

import com.badlogic.gdx.utils.ObjectMap;

public class GTOList{

	GameTextObject gto;
	AnswerButton ab;
	AnswerButtonListener abl;
	
	public GTOList(ObjectMap<Integer, GameTextObject> map){
		gto = new GameTextObject();
		gto.setId(0, 1);
		gto.setBgFloor("SciFi_VN_Background_Element_8");
		gto.setText("A long time ago in a galaxy far, far away...\n\n"
				+ "Mankind has noted the explosion of one uninhabited planet. Of course, humans have raised the question - what happened?\n"
				+ "Furthermore, some of the fragments flew toward Earth - the capital of the human empire.");
		map.put(gto.getId(), gto);
		
		gto = new GameTextObject();
		gto.setId(1, 2);
		gto.setBgFloor("ship_fleet");
		gto.setBgWall("ship-future");
		gto.setText("Nearest fleet had been sent to intercept fragments of the planet, flying in the direction of the Earth.\n"
				+ "Some time later fleet's scouts reach the fragments.\n"
				+ "They found strange rocks among pieces of planet and took several rocks on board for further examination.");
		map.put(gto.getId(), gto);
		
		gto = new GameTextObject();
		gto.setId(2);
		gto.setBgFloor("SciFi_VN_Background_Element_8");
		gto.setBgWall("spaceelevator-fleet");
		gto.setText("But on the one of the scouts, which took rocks, an accident happened. Crew member touch the rock and activate it's secret - micropores were opened and the virus was released.\n"
				+ "\n\n"
				+ "You have $points point{'$points=equal 1'?'':'s'} for virus upgrades.{'$points=above 0'?' Choose wisely.':''}");
		ab = new AnswerButton(2);
		/*ab.setText("Stealthy (2p) - hard to reveal infestation.");
		ab.setAction("$points=dec 2; $stealthy=set true;");
		ab.setCondition("$points=above 1; $stealthy=equal false;");
		gto.addAnswer(ab);
		ab = new AnswerButton(2);
		ab.setText("Strong (2p) - hard to cure and clean infestation.");
		ab.setAction("$points=dec 2; $strong=set true;");
		ab.setCondition("$points=above 1; $strong=equal false;");
		gto.addAnswer(ab);*/
		ab = new AnswerButton(2);
		ab.setText("Mobile (1p) - spores are movable.");
		ab.setAction("$points=dec 1; $mobile=set true;");
		ab.setCondition("$points=above 0; $mobile=equal false;");
		gto.addAnswer(ab);
		ab = new AnswerButton(2);
		ab.setText("Infestation (1p) - spores can be used to create infestation in the room.");
		ab.setAction("$points=dec 1; $infestation=set true;");
		ab.setCondition("$points=above 0; $infestation=equal false;");
		gto.addAnswer(ab);
		ab = new AnswerButton(2);
		ab.setText("Unoxy (1p) - virus survives in vacuum.");
		ab.setAction("$points=dec 1; $unoxy=set true;");
		ab.setCondition("$points=above 0; $unoxy=equal false;");
		gto.addAnswer(ab);
		ab = new AnswerButton(2);
		ab.setText("Strange behavior (1p) - virus can couse victims to action.");
		ab.setAction("$points=dec 1; $mind_controll=set true;");
		ab.setCondition("$points=above 0; $mind_controll=equal false;");
		gto.addAnswer(ab);
		ab = new AnswerButton(2);
		ab.setText("Aggressive (1p) - victims attack other lifeforms to spread the infection.");
		ab.setAction("$points=dec 1; $aggressive=set true;");
		ab.setCondition("$points=above 0; $aggressive=equal false;");
		gto.addAnswer(ab);
		ab = new AnswerButton(3);
		ab.setText("Done.");
		gto.addAnswer(ab);
		map.put(gto.getId(), gto);
		
		gto = new GameTextObject();
		gto.setId(3);
		gto.setBgFloor("redshiprbg");
		gto.setBgWall(null);
		gto.setText("What to do?");
		map.put(gto.getId(), gto);
		
		
		
		/*
		gto = new GameTextObject();
		gto.setId();
		gto.setBgFloor("SciFi_VN_Background_Element_8");
		gto.setBgWall("ship-future");
		gto.setText("this is some text.f sf lsfl jalf alg lsga jg ldkg al;gsdg\n"
				+ " sdg sdg\n"
				+ "sdg sdh sdh sdfhsdfh sdhsdh\n");
		ab = new AnswerButton(1);
		ab.setText("First answer");
		gto.addAnswer(ab);
		ab = new AnswerButton(1);
		ab.setText("Second answer");
		gto.addAnswer(ab);
		map.put(gto.getId(), gto);
		*/
	}

}
