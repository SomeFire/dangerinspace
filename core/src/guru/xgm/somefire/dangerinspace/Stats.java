package guru.xgm.somefire.dangerinspace;

import com.badlogic.gdx.utils.ObjectMap;

public class Stats{

	public ObjectMap<String, Object> map;
	
	public Stats(){
		map = new ObjectMap<String, Object>();
		
		map.put("points", 2);
		
		map.put("stealthy", false);//2 ������ ����������
		map.put("strong", false);//2 ������ ����������
		map.put("mobile", false);//1 ������������� ��������������
		map.put("infestation", false);//1 ������� � �������, ������� ����������� ���������
		map.put("unoxy", false);//1 �������� ��� ���������
		map.put("mind_controll", false);//1 ����� ������������ ��������� ��� ���������� ��������
		map.put("aggressive", false);//1 ������� � �������
		
		map.put("alarm", false);//����� ���������

		map.put("generatorIsActive", true);
		map.put("generatorHP", 100);
		map.put("oxygenIsActive", true);
		map.put("oxygenHP", 100);
		map.put("medbayHP", 100);
		map.put("engineIsActive", true);
		map.put("engineHP", 100);
		map.put("pilotControlHP", 100);
	}

}
