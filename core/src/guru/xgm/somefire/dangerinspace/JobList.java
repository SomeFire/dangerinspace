package guru.xgm.somefire.dangerinspace;

import java.util.Random;

import com.badlogic.gdx.utils.Array;

public class JobList{

	private Array<String> locationName;
	private Array<Integer> locationChance;
	private int max;
	
	public JobList(){
		locationName = new Array<String>();
		locationChance = new Array<Integer>();
		max = 0;
	}
	
	public void add(String location, int chance){
		locationName.add(location);
		locationChance.add(chance);
		max+=chance;
	}
	
	public String getRandomJob(){
		int i = new Random().nextInt(max) - locationChance.get(0);
		int k = 0;
		while (i>0 && k<locationChance.size){
			k++;
			i -= locationChance.get(k);
		}
		return locationName.get(k);
	}

}
