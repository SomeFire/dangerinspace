package guru.xgm.somefire.questUI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.scenes.scene2d.Actor;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Image;
import com.badlogic.gdx.scenes.scene2d.utils.TextureRegionDrawable;

public class Background{

	private Image
		floor,
		wall;
	private Stage stage;
	
	public Background(Stage s, TextureRegion floor, TextureRegion wall){
		stage = s;
		this.floor = (floor!=null)?new Image(floor):new Image();
		this.wall = (wall!=null)?new Image(wall):new Image();
		setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		stage.addActor(this.floor);
		stage.addActor(this.wall);
	}
	
	public Background(Stage s, TextureRegion background){
		this(s, background, null);
	}
	
	public void setBounds(float x, float y, float width, float height){
		floor.setBounds(x, y, width, height);
		wall.setBounds(x, y, width, height);
	}

	public void setBackground(TextureRegion floor, TextureRegion wall){
		if (floor!=null)
			this.floor.setDrawable(new TextureRegionDrawable(floor));
		else this.floor.setDrawable(null);
		if (wall!=null)
			this.wall.setDrawable(new TextureRegionDrawable(wall));
		else this.wall.setDrawable(null);
	}
	
	public void setBackground(TextureRegion background){
		setBackground(background, null);
	}
	
	public Image getWall(){
		return wall;
	}
}