package guru.xgm.somefire.questUI;

import com.badlogic.gdx.utils.Array;

public class GameTextObject{

	private int
		id,
		nextId;
	private String
		text,
		bgFloor,
		bgWall;
	private Array<AnswerButton> answers;
	
	public GameTextObject(){
		answers = new Array<AnswerButton>();
	}

	public int getId(){
		return id;
	}

	public int getNextId(){
		return nextId;
	}

	public void setId(int id, int nextId){
		this.id = id;
		this.nextId = nextId;
	}
	
	public void setId(int id){
		this.id = id;
		this.nextId = -1;
	}

	public String getText(){
		return text;
	}

	public void setText(String text){
		this.text = text;
	}
	
	public Array<AnswerButton> getAnswers(){
		return answers;
	}
	
	public void addAnswer(AnswerButton answer){
		answers.add(answer);
	}

	public String getBgFloor(){
		return bgFloor;
	}

	public void setBgFloor(String bgFloor){
		this.bgFloor = bgFloor;
	}

	public String getBgWall(){
		return bgWall;
	}

	public void setBgWall(String bgWall){
		this.bgWall = bgWall;
	}
}
