package guru.xgm.somefire.questUI;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.utils.Align;

public class TextWindow{

	private Stage stage;
	private Skin skin;
	
	private Table t;
	private ScrollPane sp;
	private Label l;
	
	public TextWindow(Stage s, Skin sk, float x, float y, float width, float height){
		stage = s;
		skin = sk;
		
		t = new Table(skin);
		t.setBounds(x, y, width, height);
		t.align(Align.bottomLeft);
		stage.addActor(t);

		l = new Label("", skin);
		l.setWrap(true);
		l.setAlignment(Align.topLeft);
		
		sp = new ScrollPane(l, skin, "dis");
		sp.setFillParent(true);
		sp.setupOverscroll(0, 0, 0);
		t.add(sp);
	}

	public void setText(String text){
		l.setText(text);
	}
	
	public void setY(float y){
		t.setBounds(0, y, t.getWidth(), t.getTop()-y);
	}
}
