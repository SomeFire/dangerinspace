package guru.xgm.somefire.questUI;

import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;

public class AnswerButtonListener extends ClickListener{

	AnswerButton button;
	
	public AnswerButtonListener(AnswerButton b){
		button = b;
	}

	@Override
	public void clicked (InputEvent event, float x, float y) {
		if (QUtils.selectedAnswer != null) QUtils.selectedAnswer.setChecked(false);
		if (button.isChecked()) QUtils.selectedAnswer = button;
		else QUtils.selectedAnswer = null;
	}
}
