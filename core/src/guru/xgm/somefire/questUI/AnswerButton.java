package guru.xgm.somefire.questUI;

import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class AnswerButton extends TextButton{

	private AnswerButtonListener listener;
	private int nextSlideID;
	private String
		action,
		condition;

	public AnswerButton(int id){
		super("", QUtils.skin, "toggle");
		nextSlideID = id;
		getLabel().setAlignment(QUtils.answerAlignment);
		addListener(listener = new AnswerButtonListener(this));
	}

	//super.setText(String text);
	
	public int getNextSlideID(){
		return nextSlideID;
	}
	
	public void setAction(String text){
		action = text;
	}
	
	public String getAction(){
		return action;
	}
	
	public void setCondition(String text){
		condition = text;
	}
	
	public String getCondition(){
		return condition;
	}
}
