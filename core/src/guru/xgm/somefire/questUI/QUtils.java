package guru.xgm.somefire.questUI;

import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.utils.Align;

public class QUtils{

	public static Skin skin;
	
	public static QScreen gameScreen;
	
	public static AnswerButton selectedAnswer;
	
	public static int answerAlignment = Align.left;

}
