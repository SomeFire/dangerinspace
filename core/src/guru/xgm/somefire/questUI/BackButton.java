package guru.xgm.somefire.questUI;

import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;

public class BackButton extends TextButton{

	public BackButton(String text, Skin skin){
		super(text, skin);
	}

}
