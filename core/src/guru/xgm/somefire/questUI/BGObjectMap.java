package guru.xgm.somefire.questUI;

import com.badlogic.gdx.utils.ObjectMap;

public class BGObjectMap<K, V> extends ObjectMap<K, V>{

	@Override
	public V get(K key){
		return (key!=null) ? super.get(key) : null;
	}
}
